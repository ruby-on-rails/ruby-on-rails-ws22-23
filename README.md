# ruby-on-rails-ws22-23

Geteiltes Repository für die Veranstaltung Ruby on Rails im WS 22/23. Hier teilen wir Foliensätze, Übungsaufgaben und Infoblätter mit Ihnen.

Die Abgabe der Projektarbeit wird ebenfalls hier stattfinden. Erstellen Sie dazu ein Unterverzeichnis mit ihrem THM-Kürzel (bspw. tsnh07).
