class Creature
  def initialize options = {}
    @name = "Anonymous Creature"
    @str = 1
    @con = 1
    @dex = 1
    @int = 1
    @lvl = 1
    @exp = 0
    @hp = 2 * @con + 1 * @str
  end
  attr_reader :name
  attr_reader :lvl
  attr_reader :exp

  # def name
  # @name
  # end

  # def lever
  # @level
  # end

  # def exp
  # @exp
  # end

  def alive?
    if @hp > 0
      true
    else
      false
    end
  end

  def wound(damage)
    @hp = @hp - damage
  end

  def attack(enemy)
    damage = @str + @str * rand(3)
    enemy.wound(damage)
  end

  def to_s
    puts(@name + @str + @con + @dex + @int + @lvl + @exp + @hp).to_s.join(" ")
  end
end
