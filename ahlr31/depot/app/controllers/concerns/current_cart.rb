module CurrentCart
  private
  #Methodendefinition
  def set_cart
    #gibt es Kart?
    # wenn ja: ()sucht record aus datenbank
    # wenn id nicht gibt exception
    @cart = Cart.find(session[:cart_id])
    #dann rescue; shortcut
  rescue ActiveRecord::RecordNotFound
    #create erstellt
    @cart = Cart.create
    #wenn cart gefunden:
    # neuer Einkaufswagen definiert und gespeichert
    # neuer Kart in Session rein
    session[:cart_id] = @cart.id
  end
end
