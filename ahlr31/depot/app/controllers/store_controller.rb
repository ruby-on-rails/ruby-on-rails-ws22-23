class StoreController < ApplicationController
  include CurrentCart
  before_action :set_cart
  def index
    #Product Model; sortiert nach Titel
    # in Instanzvariable rein
    @products = Product.order(:title)
  end
end
