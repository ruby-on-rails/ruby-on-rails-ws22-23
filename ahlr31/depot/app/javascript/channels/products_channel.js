import consumer from "./consumer"

consumer.subscriptions.create("ProductsChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
    console.log("ProductsChannel connected")
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
    console.log("ProductsChannel disconnected")
  },

  received(data) {
    console.log("DataRecieved")
    const storeElement = document.querySelector("main.store")
    if (storeElement) {
      storeElement.innerHTML = data.html
    }
  }
});
