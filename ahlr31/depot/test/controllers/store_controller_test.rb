require "test_helper"

class StoreControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get store_index_url
    # 4 SideTags mit 4 Links; sind 4 Links in Sidebar?
    assert_response :success
    assert_select 'nav.side_nav a', minimum: 4
    assert_select 'main ul.catalog li', 3
    # Überschrift
    assert_select 'h2', 'Programming Ruby 1.9'
    # 1 Symbol Komma Zahl; +: mehrere davon (vor komma(Trennkomma 1.000)) dann . und 2 Leerzeichen
    assert_select '.price', /[,\d]+\.\d\d €/
  end
end
