# Reviewseite

Auf dieser Seite können Reviews zu Spielen gepostet werden.

# Andorderungen:

#### Must Have: 

-Konto: Nutzer können ein Konto anlegen. (Devise)

-Adminbereich: Es muss eine Adminseite existieren,
auf der Rechte von Nutzern bestimmt werden. (Rails-Admin, Administrate)

-Titelseite: Ein Wilkommenbereich,
außerdem sollen einige Artikel ausgegeben werden.

(-Kategorien: Spiele werden in Kategorien eingeteilt (Genres, Singleplayer/Multiplayer).
Basierend darauf kann man sich durch die Seite klicken und Spiele finden.)

-Suchfunktion: Man kann Spiele nach Namen,
Genres und Bewertung suchen.

-Spieleseiten: Dabei handelt es sich um Profilseiten für Spiele,
welche den Namen, das Bild, eine Review sowie eine Beschreibung enthält.

-Bewertung: Es kann eine durchschnittliche Bewertung basierend auf den Meinungen der Nutzer erstellt
und angezeigt werden.


#### Nice-To Have:

-weitere Genres/ mehrere " pro Spiel <br>

-Profile: Erstellung eines Profils. <br>


# Setup(App)
1. bundle install

2. rails yarn install

3. rails db:migrate

4. rails db:seed

# Setup (Sign_up/kontoerstellung)
Die Funktionen eine Review zu posten oder zu bearbeiten stehen nur einem Nutzer mit Adminrechnten zur Verfügung. <br>
Nutzer werden standardmäßig ohne Adminrechte erstellt. <br>
Derzeit existiert ein User mit Adminrechten in den Seeds. <br>
Seine Nutzerdaten lauten: Name: einStudent@thm.de Passwort: 1234qwer <br>
Weiter Nutzer mit Adminrechten können nur durch eine neue Seed erstellt werden. <br>
Dafür müssen Sie folgendes tun: <br>
in den Seeds eintippen: <br>
"username eingeben" = User.create!( email: 'ihre Email', password: 'ihr Passwort', admin: true) <br>
ausführen: <br>
rails db:seed <br>

# Bedienungshinweise
Alle Seiten bis auf die Log_in Seite können nur nach einer Anmeldung eingesehen werden. <br>
Alle Funktionen bis auf eine Kontoersetllung können nur nach einer Anmeldung genutzt werden.<br>
Die Homepage bietet eine zufällige Review.<br>
Die Navigation erfolgt über die Navigationsleiste oben:<br>
Es gibt dort einen Link zur Homepage und zu einer Liste aller Reviews.<br>
Die Liste aller Reviews kann durch das anklicken des Namens in alphabetisch aufsteigender oder abfallender Reihenfolge nach dem Namen sortiert werden.<br>
Eine Review kann durch das Anklicken des Namens und des Klickens des Show-Knopfes geöffnet werden.<br>
Dort befinden sich 2 Feld34 für das Kommentieren der Review. <br>
Im ersten wird der Name des Kommentatoren und im zweiten der Inhalt des Kommentars angegeben. <br>
**Dies funktioniert aber nicht!** <br>
Über der Liste der Reviews ist eine Suchleiste, in welcher nach Einträgen in Namen und Inhalten von Reviews gesucht werden kann.<br>

# Quellen
https://stackoverflow.com/questions/47735830/user-must-exist-error-message-in-rails <br>
https://www.thoughtco.com/rails-blog-tutorial-allowing-comments-2908216 <br>
https://human-se.github.io/rails-demos-n-deets-2021/demos/destroy-actions/ <br>
https://medium.com/@byron.skoutaris/how-to-set-up-devise-gem-a8610be46d83 <br>
https://www.digitalocean.com/community/tutorials/how-to-set-up-user-authentication-with-devise-in-a-rails-7-application <br>
https://www.digitalocean.com/community/tutorials/how-to-add-bootstrap-to-a-ruby-on-rails-application <br>
https://guides.rubyonrails.org/v6.1/association_basics.html <br>
https://guides.rubyonrails.org/layouts_and_rendering.html <br>
https://www.youtube.com/watch?v=qWObWACNY9g <br>
https://www.youtube.com/watch?v=7v2EMmfBJL8 <br>