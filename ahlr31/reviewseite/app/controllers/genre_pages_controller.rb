class GenrePagesController < ApplicationController
  before_action :set_genre_page, only: %i[ show edit update destroy ]

  # GET /genre_pages or /genre_pages.json
  def index
    @genre_pages = GenrePage.all
  end

  # GET /genre_pages/1 or /genre_pages/1.json
  def show
  end

  # GET /genre_pages/new
  def new
    @genre_page = GenrePage.new
  end

  # GET /genre_pages/1/edit
  def edit
  end

  # POST /genre_pages or /genre_pages.json
  def create
    @genre_page = GenrePage.new(genre_page_params)

    respond_to do |format|
      if @genre_page.save
        format.html { redirect_to genre_page_url(@genre_page), notice: "Genre page was successfully created." }
        format.json { render :show, status: :created, location: @genre_page }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @genre_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /genre_pages/1 or /genre_pages/1.json
  def update
    respond_to do |format|
      if @genre_page.update(genre_page_params)
        format.html { redirect_to genre_page_url(@genre_page), notice: "Genre page was successfully updated." }
        format.json { render :show, status: :ok, location: @genre_page }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @genre_page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /genre_pages/1 or /genre_pages/1.json
  def destroy
    @genre_page.destroy

    respond_to do |format|
      format.html { redirect_to genre_pages_url, notice: "Genre page was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_genre_page
      @genre_page = GenrePage.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def genre_page_params
      params.fetch(:genre_page, {})
    end
end
