class Comment < ApplicationRecord
  belongs_to :review
  belongs_to :user
  validates_presence_of :email
  validates_length_of :email, :within => 2..20
  validates_presence_of :body
end
