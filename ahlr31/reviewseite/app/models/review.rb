class Review < ApplicationRecord
  has_many :comments, :dependent => :destroy
  has_and_belongs_to_many :genres
  belongs_to :user

  #def name_or_body(auth_object = nil)
  # ["name_or_body"]
  #end
  # # def self.ransackable_attributes(auth_object = nil)
  #   #   super & %w(name body)
  #   # end
  def self.ransackable_attributes(auth_object = nil)
    ["name", "body"]
  end
  def self.ransackable_associations(auth_object = nil)
    []
  end
end
