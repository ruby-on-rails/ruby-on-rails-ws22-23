json.extract! genre_page, :id, :created_at, :updated_at
json.url genre_page_url(genre_page, format: :json)
