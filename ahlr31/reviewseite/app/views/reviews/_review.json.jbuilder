json.extract! review, :id, :name, :genre, :body, :picture_id, :created_at, :updated_at
json.url review_url(review, format: :json)
