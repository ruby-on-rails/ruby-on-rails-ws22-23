Rails.application.routes.draw do
  namespace :admin do
      resources :users
      resources :comments
      resources :genres
      resources :reviews

      root to: "users#index"
    end
  resources :searches
  resources :genre_pages
  resources :genres
  resources :comments
  resources :reviews
  get 'home/index'
  get 'review_pages/show/:id' => 'review_pages#show', as: 'review_page'
  devise_for :users
  root to: 'home#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
