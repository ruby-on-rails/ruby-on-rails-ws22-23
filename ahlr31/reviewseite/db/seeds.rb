# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Comment.delete_all
Review.delete_all
Genre.delete_all
User.delete_all


user_1 = User.create!( email: 'einStudent@thm.de', password: '1234qwer', admin: true)

review_1 = Review.create!(name: 'Batman: Arkham Asylum', body: "Ein fantasitisches Action-Spiel", user_id: user_1.id)
review_2 = Review.create!(name: 'Total War: Shogun II', body: 'Ein fantastisches Strategie-Spiel', user_id: user_1.id)

genre_1 = Genre.create!(name: 'Action')
genre_2 = Genre.create!(name: 'Strategie')

review_1.genres << genre_1
review_2.genres << genre_2

comment_1 = Comment.create!(email: 'einStudent@thm.de', body: "Ein fantasitisches Action-Spiel", review_id: review_1.id, user_id: user_1.id)
comment_2 = Comment.create!(email: 'einStudent@thm.de', body: 'Ein fantastisches Strategie-Spiel', review_id: review_2.id, user_id: user_1.id)