class Review < ApplicationRecord
  has_and_belongs_to_many :genres

  def self.ransackable_attributes(auth_object = nil)
    ["body", "title"]
  end

  def self.ransackable_associations(auth_object = nil)
    []
  end

end
