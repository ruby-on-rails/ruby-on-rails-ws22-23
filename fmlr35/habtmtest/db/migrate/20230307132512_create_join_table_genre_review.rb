class CreateJoinTableGenreReview < ActiveRecord::Migration[6.1]
  def change
    create_join_table :genres, :reviews do |t|
      # t.index [:genre_id, :review_id]
      # t.index [:review_id, :genre_id]
    end
  end
end
