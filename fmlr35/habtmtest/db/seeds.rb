# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Genre.delete_all
Review.delete_all

genre_1 = Genre.create! name: "Action"
genre_2 = Genre.create! name: "Strategy"
genre_3 = Genre.create! name: "Shooter"

review_1 = Review.create! title: "Review 1", genres: [genre_1, genre_2, genre_3]
review_2 = Review.create! title: "Review 2", genres: [genre_1, genre_2]
review_3 = Review.create! title: "Review 3", genres: [genre_1]
