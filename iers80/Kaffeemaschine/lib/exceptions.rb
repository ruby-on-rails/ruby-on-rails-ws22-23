class NotEnoughWaterLeftError< Exception

end

class NotOnlineError < Exception

end

class DrinkNotAvailable < Exception

end

