require_relative ("exceptions.rb")
class KaffeeMaschine
  CupSize=1
  def initialize()
    @wasserfuellstand=3
    @isOn=false

  end
  def einschalten
    puts("Kaffeemaschine ist eingeschaltet!\n")
    @isOn=true
  end
  def koche_kaffee(n,drink: :tee)
    if !@isOn
      raise NotOnlineError,"It's off"
    else
      if n<=@wasserfuellstand
        if drink==:tee
          puts("🍵 🍵")
          @wasserfuellstand=@wasserfuellstand-n
        elsif drink==:himbeer
          puts("🍺 🍺")
          @wasserfuellstand=@wasserfuellstand-n
        elsif drink==:kaffee
          puts("☕ ☕")
          @wasserfuellstand=@wasserfuellstand-n
        else
          raise DrinkNotAvailable,"We do not have that drink"
        end
      else
        raise NotEnoughWaterLeftError,"An error occurred because there is not enough water left"
      end
    end
  end


end
