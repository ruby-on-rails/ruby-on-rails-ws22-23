class Creature
  @name
  @str
  @con
  @int
  @lvl
  @exp
  @hp
  @dex
  def initialize(options={})
    @name = "Anonymous Creature"
    @str = 1
    @con = 1
    @dex=1
    @int = 1
    @lvl = 1
    @exp = 0
    @hp = 2 * @con + 1 * @str
  end
  def name
    @name
  end
  def lvl
    @lvl
  end
  def exp
    @exp
  end
  def alive?
    if @hp>0
      return true
    else
      return false
    end
  end
  def wound(damage)
    @hp=@hp-damage
  end
  def attack(enemy)
    damage = @str + @str * rand(3)
    if enemy==self
      raise ArgumentError,"self attack not allowed"
    else
      if enemy.is_a? Creature
        enemy.wound(damage)
      else
        raise ArgumentError,"wrong type of enemy"
      end
    end
  end

  def to_s
    "#{@name} Level #{@lvl}\nHP = #{@hp}\nEXP = #{@exp}\nSTR = #{@str}\nCON = #{@con}\nDEX = #{@dex}\nINT = #{@int}"
  end


end