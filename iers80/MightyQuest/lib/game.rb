require_relative "hero"
require_relative "monster"
require_relative "creature"
module Game

  def self.fight(hero,monster)
    while hero.alive? && monster.alive?
      monster.attack(hero)
      hero.attack(monster)
    end
  end
  def self.create_monster
    Monster.new()
  end
  def self.create_monsters
    arrayOfMonster=[]
    10.times do
      arrayOfMonster.push(create_monster)
    end
    arrayOfMonster

  end


end