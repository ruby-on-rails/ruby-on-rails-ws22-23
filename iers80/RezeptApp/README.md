# RezepteApp
## Beschreibung
Die Rezept-App ist eine Anwendung, mit der Benutzer ihre eigenen Rezepte erstellen 
, Rezepte anderer Benutzer durchsuchen und Rezepte als Favoriten markieren können. Die App ist in Kategorien unterteilt, um die Suche nach bestimmten Arten von Rezepten zu erleichtern, wie z.B. Frühstück, Mittagessen und Abendessen.
## User Stories
### must to have
- [x] Ich möchte neues Konto erstellen und mich anmelden.
- [x] Ich möchte mein eigenes Rezept erstellen, sodass andere Benutzer auf meinem Rezept zugreifen kann.
- [x] Ich möchte die Rezepte beim Homepage sehen, damit ich entscheiden kann, was ich zubreiten möchte.
- [x] Ich möchte verchiedene Kategorien(Abendessen, Nachtisch, Frühstück) sehen.
- [x] Ich möchte die Rezepte als favorite speichern, sodass ich einfacher darauf zugreifen kann.
- [x] Ich möchte die Schritte sehen, sodass ich weiß, wie man das Essen zubreiten kann.
- [x] Ich möchte eine Zutatenliste sehen, damit ich weiß, was ich für die Zubereitung brauche.
### nice to have
- [ ] Ich möchte nach die Rezepte(Filtern nach Kategorien) suchen.
- [ ] Ich möchte auf die Rezepte kommentieren.
- [ ] Ich möchte das sehen, wie viele Kalorie und wie viele Protein das Rezept hat und ob es Vegan ist.

## Installation
````
bundle install
yarn install
rails db:migrate
rails db:seed
rails s

````

## Test User Info
1. Email: test@test.com  <br>
Password: 123456
2. Email: test1@test1.com  <br>
Password: 123456
3. Email: test2@test2.com  <br>
Password: 123456

## Dependencies
* **Devise** : Authentifizierung von Benutzern
* **bootstrap** : Frontend-Entwicklung 
* **faker** : Testdaten generieren
* **acts_as_favoritor** : Favoritenobjekte implementieren
* **actiontext** : Rich-Text implementieren
* **font-awesome-sass** : Vektor-Icons

## Quellen
* [Bootstrap Installation ](https://gist.github.com/nachozt/799a54c1bba79975bdcfd4f67226ce22)
* [Dynamic Like & Dislike Buttons ](https://medium.com/swlh/add-dynamic-like-dislike-buttons-to-your-rails-6-application-ccce8a234c43)
* [Dynamically add input field](https://rbudiharso.wordpress.com/2010/07/07/dynamically-add-and-remove-input-field-in-rails-without-javascript/)





