class FavoritesController < ApplicationController
  before_action :authenticate_user!, only: :index
  def index
    @users=User.all
    @recipes = current_user.favorited_by_type('Recipe')
  end
end
