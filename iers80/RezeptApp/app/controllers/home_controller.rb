class HomeController < ApplicationController
  before_action :authenticate_user!, only: :toggle_favorite

  def toggle_favorite
    @recipe = Recipe.find_by(id: params[:id])
    current_user.favorited?(@recipe)  ?current_user.unfavorite(@recipe) : current_user.favorite(@recipe)
  end
  def index
    @users=User.all
    @recipes = Recipe.all
  end
end
