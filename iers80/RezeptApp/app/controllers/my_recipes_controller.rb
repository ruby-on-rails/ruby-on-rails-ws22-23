class MyRecipesController < ApplicationController
  before_action :authenticate_user!
  def index
    @users=User.all
    @user = current_user
    @recipes = @user.recipes
  end

end
