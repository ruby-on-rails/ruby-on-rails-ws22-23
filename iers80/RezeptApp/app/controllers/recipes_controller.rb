class RecipesController < ApplicationController
  before_action :set_recipe, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, only: :toggle_favorite

  def toggle_favorite
    @recipe = Recipe.find_by(id: params[:id])
    current_user.favorited?(@recipe)  ?current_user.unfavorite(@recipe) : current_user.favorite(@recipe)
  end


  # GET /recipes or /recipes.json
  def index
    @recipes = Recipe.all
    @recipes.ingredients.build
  end

  # GET /recipes/1 or /recipes/1.json
  def show
    @recipes = Recipe.find(params[:id])

    @ingredients =  Ingredient.where(recipe_id: params[:id]).all
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
    @recipe.ingredients.build
  end

  # GET /recipes/1/edit
  def edit
  end



  # POST /recipes or /recipes.json
  def create
    @recipe = Recipe.new(recipe_params)
    if params[:add_ingredient]
      # add empty ingredient associated with @recipe
      @recipe.ingredients.build
    elsif params[:remove_ingredient]
      # nested model that have _destroy attribute = 1 automatically deleted by rails
    else
      # save goes like usual
      if @recipe.save
        flash[:notice] = "Successfully created recipe."
        redirect_to @recipe and return
      end
    end
    render :action => 'new'
  end

  # PATCH/PUT /recipes/1 or /recipes/1.json
  def update
    respond_to do |format|
      if @recipe.update(recipe_params)
        format.html { redirect_to recipe_url(@recipe), notice: "Recipe was successfully updated." }
        format.json { render :show, status: :ok, location: @recipe }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1 or /recipes/1.json
  def destroy
    @recipe.destroy
    respond_to do |format|
      format.html { redirect_to my_recipes_url, notice: "Recipe was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def recipe_params
      params.require(:recipe).permit(:titel, :description, :user_id, :category_id,:directions,:recipeImage,:content,ingredients_attributes:[:text])
    end
end
