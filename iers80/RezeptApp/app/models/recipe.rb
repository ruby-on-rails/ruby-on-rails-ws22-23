class Recipe < ApplicationRecord
  validates :titel, :description,:directions, presence: true
  has_rich_text :content
  belongs_to :category
  belongs_to :user
  has_many :ingredients, :dependent => :delete_all
  accepts_nested_attributes_for :ingredients
  has_one_attached :recipeImage
  acts_as_favoritable

end
