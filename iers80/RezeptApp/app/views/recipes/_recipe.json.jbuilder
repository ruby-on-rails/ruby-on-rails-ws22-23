json.extract! recipe, :id, :titel, :description, :image_url, :ingredients, :created_at, :updated_at
json.url recipe_url(recipe, format: :json)
