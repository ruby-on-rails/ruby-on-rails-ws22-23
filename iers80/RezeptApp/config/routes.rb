Rails.application.routes.draw do

  resources :ingredients
  resources :my_recipes

  resources :recipes do
    member do
      post 'toggle_favorite', to: "recipes#toggle_favorite"
    end
  end

  resources :favorites


  resources :categories do
    member do
      get :show_recipes
    end
  end

  devise_for :users
  # resource :user, only: [] do
  #   root 'home#index'
  #
  # end
  root 'home#index' do
    member do
      post 'toggle_favorite', to: "recipes#toggle_favorite"
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
