class CreateRecipes < ActiveRecord::Migration[6.1]
  def change
    create_table :recipes do |t|
      t.string :titel
      t.string :description
      t.string :image_url
      t.string :ingredients

      t.timestamps
    end
  end
end
