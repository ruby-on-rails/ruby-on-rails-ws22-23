#---
# Excerpted from "Agile Web Development with Rails 6",
# published by The Pragmatic Bookshelf.
# Copyrights apply to this code. It may not be used to create training material,
# courses, books, articles, and the like. Contact us if you are in doubt.
# We make no guarantees that this code is fit for any purpose.
# Visit http://www.pragmaticprogrammer.com/titles/rails6 for more book information.
#---
# encoding: utf-8
# User.delete_all
# user=User.new(
#   email: "test@test.com",
#   password: "123456"
# )
# user.save!
# user=User.new(
#   email: "test2@test2.com",
#   password: "123456"
# )
# user.save!
# user=User.new(
#   email: "test3@test3.com",
#   password: "123456"
# )
#
# user.save!
# p "Created #{User.count} User"



# generate 3 recipes for Abendessen and for user1
# 3.times do
#   Recipe.create!(
#     titel: Faker::Lorem.words(number: rand(2..10)),
#     ingredients: Faker::Markdown.ordered_list,
#     description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: true, random_sentences_to_add: 4),
#     image_url: 'recipe1.jpg',
#     category_id:2,
#     user_id:2
#   )
#   p "Created #{Recipe.count} recipes"
# end
#
# # generate 3 recipes for Frühstück and for user1
# 3.times do
#   Recipe.create!(
#     titel: Faker::Lorem.words(number: rand(2..10)),
#     ingredients: Faker::Markdown.ordered_list,
#     description: Faker::Lorem.paragraph(sentence_count: 2, supplemental: true, random_sentences_to_add: 4),
#     image_url: 'recipe1.jpg',
#     category_id:1,
#     user_id:2
#   )
#   p "Created #{Recipe.count} recipes"
# end
Ingredient.delete_all
p "Deleted Ingredient"
Recipe.delete_all
p "Deleted Recipe"
Category.delete_all
p "Deleted Category"
User.delete_all
p "Deleted User"



#create users
user=User.new(
  id:1,
  email: "test@test.com",
  password: "123456"
)
user.save!
user=User.new(
  id:2,
  email: "test2@test2.com",
  password: "123456"
)
user.save!
user=User.new(
  id:3,
  email: "test3@test3.com",
  password: "123456"
)

user.save!
p "Created #{User.count} User"

#create categories
Category.create!(
  id:1,
  title: "Frühstück",
  image_url: 'recipe1.jpg',
  description: "Frühstück ist die wichtigste Mahlzeit des Tages und bietet die perfekte Gelegenheit,
                um den Körper mit Nährstoffen und Energie zu versorgen, um den Tag zu beginnen.
                Diese Kategorie umfasst eine Vielzahl von leckeren und nahrhaften Speisen, die speziell
                für den Morgen entwickelt wurden. Hier finden Sie eine große Auswahl an Optionen, von
                herzhaften Speck- und Eiergerichten bis hin zu süßen Pfannkuchen und frischen Obstsalaten.
                Auch verschiedene Getränke wie Kaffee, Tee oder frisch gepresste Säfte können in dieser
                Kategorie zu finden sein. Egal, ob Sie nach einem schnellen und einfachen Frühstück oder
                einem ausgiebigen Brunch suchen, hier werden Sie garantiert fündig."
)
p "Created #{Category.count} categories"
Category.create!(
  id:2,
  title: "Abendessen",
  image_url: 'recipe1.jpg',
  description:"Abendessen ist eine Gelegenheit, um den Tag mit einem köstlichen und sättigenden Mahlzeit
               abzuschließen. In dieser Kategorie finden Sie eine Vielzahl von herzhaften und leckeren Gerichten,
               die speziell für den Abend entwickelt wurden. Hier können Sie aus einer breiten Auswahl an Optionen
               wählen, von warmen Suppen und Aufläufen bis hin zu Fleisch-, Fisch- oder vegetarischen Gerichten.
               Es gibt auch viele Beilagen und Salate, um das Abendessen zu komplettieren. Das Abendessen ist oft
               ein Zeitpunkt, um mit Freunden oder der Familie zusammenzukommen und zu genießen, daher gibt es
               viele Rezepte, die für größere Gruppen geeignet sind. Eine gute Möglichkeit, den Abend ausklingen
               zu lassen, ist ein Dessert oder ein Glas Wein. Egal, ob Sie auf der Suche nach einer schnellen und
               einfachen Mahlzeit oder einem aufwendigen Drei-Gänge-Menü sind, hier finden Sie alles, was Sie für
               ein leckeres Abendessen brauchen."
)
p "Created #{Category.count} categories"
Category.create!(
  id:3,
  title: "Dessert",
  image_url: 'recipe1.jpg',
  description:"Abendessen ist eine Gelegenheit, um den Tag mit einem köstlichen und sättigenden Mahlzeit
               abzuschließen. In dieser Kategorie finden Sie eine Vielzahl von herzhaften und leckeren Gerichten,
               die speziell für den Abend entwickelt wurden. Hier können Sie aus einer breiten Auswahl an Optionen
               wählen, von warmen Suppen und Aufläufen bis hin zu Fleisch-, Fisch- oder vegetarischen Gerichten.
               Es gibt auch viele Beilagen und Salate, um das Abendessen zu komplettieren. Das Abendessen ist oft
               ein Zeitpunkt, um mit Freunden oder der Familie zusammenzukommen und zu genießen, daher gibt es
               viele Rezepte, die für größere Gruppen geeignet sind. Eine gute Möglichkeit, den Abend ausklingen
               zu lassen, ist ein Dessert oder ein Glas Wein. Egal, ob Sie auf der Suche nach einer schnellen und
               einfachen Mahlzeit oder einem aufwendigen Drei-Gänge-Menü sind, hier finden Sie alles, was Sie für
               ein leckeres Abendessen brauchen."
)
p "Created #{Category.count} categories"




# generate 9 recipes of each meal for user2
3.times do |i|
  3.times do
  Recipe.create!(
      titel: Faker::Dessert.topping,
      description: Faker::Lorem.paragraph(sentence_count: 6, supplemental: true, random_sentences_to_add: 8),
      directions:Faker::Lorem.paragraph(sentence_count: 6, supplemental: true, random_sentences_to_add: 8),
      image_url: 'recipe1.jpg',
      category_id:i+1,
      user_id:2
    )
    p "Created #{Recipe.count} recipes"
  end
end

3.times do |i|
  3.times do
    Recipe.create!(
      titel: Faker::Dessert.topping,
      description: Faker::Lorem.paragraph(sentence_count: 6, supplemental: true, random_sentences_to_add: 8),
      directions:Faker::Lorem.paragraph(sentence_count: 6, supplemental: true, random_sentences_to_add: 8),
      image_url: 'recipe1.jpg',
      category_id:i+1,
      user_id:1
    )
    p "Created #{Recipe.count} recipes"
  end
end

Recipe.count.times do |i|
  5.times do |y|
    Ingredient.create!(
      text: "Zutat #{y+1}",
      recipe_id:i+1
    )
    p "Created #{i+1} Ingredient"
  end
end

# Ingredient.all.each do |ingredient|
#   ActionText::RichText.create!(record_type: 'Ingredient', record_id: ingredient.id, name: 'content', body: Faker::Lorem.sentence)
# end
#

