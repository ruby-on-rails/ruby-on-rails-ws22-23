require "test_helper"

class MyRecipesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get my_recipes_index_url
    assert_response :success
  end
end
