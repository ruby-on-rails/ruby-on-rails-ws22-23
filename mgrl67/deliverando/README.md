# Lieferdienst Web-App *deliverando*
**von Marius Gerlach**

*deliverando* ist eine Plattform , die es hungrigen Kunden erlaubt bei Restaurants Essen zu bestellen und Restaurantbesitzern eine Möglichkeit bietet ein breites Spektrum an potenziellen Kunden zu erreichen.

---
## Installation
### Wichtige Versionen
- `ruby 2.7.7`
- `rails 6.1.7.2`
### Anleitung
- `bundle install`
- `bin/rails db:migrate`
- `bin/rails db:seed`
- `bin/rails s`
- eventuell zusätzlich: `yarn add @rails/webpacker`

Der Server startet nun auf `localhost:3000`. Mit den voreingestellten Daten aus der Datei `db/seeds.rb` stehen bereits zwei Benutzeraccounts zur Verfügung:
- Der *hungrige Kunde*:
  - Email `cust@mail.com`
  - Passwort `passwort`
  - Der Kunde hat bereits eine offene Bestellung und Profildaten
- Der *Restaurantbesitzer*
  - Email `rest@mail.com`
  - Passwort `passwort`
  - Der Restaurantbesitzer hat drei Restaurants eingestellt
---
## Gems
- [x] devise (Benutzerverwaltung)
- [x] workflow (Status von Bestellungen)

---
## Features
### 'must-have'
- [x] Registrieren & Anmelden Benutzer & Restaurants
  - [x] devise gem einbinden
- [x] Hauptseite:
  - [x] Benutzer
    - [x] Restaurantübersicht
    - [x] Suche/Filter
  - [x] Betreiber
    - [x] Offene Bestellungen
    - [x] Speisekarteverwaltung
- [x] Quasi-Webshop:
  - [x] Speisekarte mit Warenkorb
  - [x] Bestellung tätigen
  - [x] Bestellung abschließen
### 'nice-to-have'
- [ ] Stempelkarte/Prämiensystem
- [ ] Speisekarte kann Optionen/Extrawünschen gerecht werden
- [ ] Bewertungsfunktion für Restaurants (Sternewertung/schriftl. Rezension)
- [ ] Feed für Favourites (Restaurant/Gericht/Location)
### 'special features' (aka *known bugs*)
- [ ] Warenkörbe werden nicht ordentlich gelöscht, wenn eine Bestellung abgeschlossen wird, sondern nur "geleert"
- [ ] Beim erfolglosen Anlegen oder Bearbeiten eines neuen Gerichts wird der Fehler übergangen

---
## Quellen
### Allgemeine Quellen
- [RailsGuides](https://guides.rubyonrails.org/v6.1/)
- [Ruby Toolbox](https://www.ruby-toolbox.com/)
- [devise Github](https://github.com/heartcombo/devise)
- [workflow Github](https://github.com/geekq/workflow-activerecord)
- [Kursmaterialien und Beispiele](https://git.thm.de/ruby-on-rails/ruby-on-rails-ws22-23)
### Spezielle Quellen
- [ActiveStorage zum Hochladen von Bildern](https://medium.com/@anaharris/how-to-add-image-upload-functionality-to-your-rails-app-9f7fc3f3d042)
- [Uniqueness von Usereingaben](https://apidock.com/rails/ActiveRecord/Validations/ClassMethods/validates_uniqueness_of)
- [CSS für Notices](https://www.computerhope.com/issues/ch001392.htm)
- [CSS für Sidebar](https://stackoverflow.com/questions/16094785/have-a-fixed-position-div-that-needs-to-scroll-if-content-overflows)
- [Timestamps formatieren](https://stackoverflow.com/questions/693823/formatting-timestamps)
### Bilder
- [Restaurant](https://www.stockunlimited.com/vector-illustration/restaurant-logo-design_1797611.html)
- [Burgerhouse](https://de.dreamstime.com/burger-logo-schnellimbisslogo-image111172842)
- [Indian](https://stock.adobe.com/search/images?k=indian+restaurant+logo&asset_id=436515149)
- [Logo](https://www.deliverando.it/)