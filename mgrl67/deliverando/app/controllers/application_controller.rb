class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
    "/restaurants/"
  end
  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update) do |params| params.permit(
      :email,
      :password,
      :current_password,
      :password_confirmation,
      :name,
      :street,
      :house_nr,
      :post_code,
      :city,
      :phone)
    end
  end
end
