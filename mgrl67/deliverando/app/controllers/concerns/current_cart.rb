module CurrentCart
  private
  def set_cart(restaurant_id)
    @cart = Cart.where(user_id: current_user.id, restaurant_id: restaurant_id).first
    if @cart.nil?
      @cart = Cart.create(user: current_user, restaurant: Restaurant.find(restaurant_id))
    end
  end
end
