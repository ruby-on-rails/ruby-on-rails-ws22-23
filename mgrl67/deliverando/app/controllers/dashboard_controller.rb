class DashboardController < ApplicationController
  def index
    @restaurants = Restaurant.where(user: params[:user_id])
  end
  def orders
    @restaurant = Restaurant.find(params[:restaurant_id])
    @orders = Order.where(restaurant: @restaurant)
  end
end
