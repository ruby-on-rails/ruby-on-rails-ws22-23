class DishCategoriesController < ApplicationController
  before_action :set_dish_category, only: %i[ show edit update destroy ]

  # GET /dish_categories or /dish_categories.json
  def index
    @dish_categories = DishCategory.all
  end

  # GET /dish_categories/1 or /dish_categories/1.json
  def show
  end

  # GET /dish_categories/new
  def new
    @dish_category = DishCategory.new
    @restaurant = Restaurant.find(params[:restaurant_id])
    @restaurant_id = params[:restaurant_id]
  end

  # GET /dish_categories/1/edit
  def edit
  end

  # POST /dish_categories or /dish_categories.json
  def create
    @dish_category = DishCategory.new(name: dish_category_params[:name])
    @restaurant = Restaurant.find(dish_category_params[:restaurant_id])

    respond_to do |format|
      if @dish_category.save
        format.html { redirect_to dish_new_path(restaurant_id: @restaurant), notice: "Kategorie erfolgreich angelegt." }
        format.json { render :show, status: :created, location: @dish_category }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @dish_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dish_categories/1 or /dish_categories/1.json
  def update
    respond_to do |format|
      if @dish_category.update(dish_category_params)
        format.html { redirect_to dish_category_url(@dish_category), notice: "Dish category was successfully updated." }
        format.json { render :show, status: :ok, location: @dish_category }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @dish_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dish_categories/1 or /dish_categories/1.json
  def destroy
    @dish_category.destroy

    respond_to do |format|
      format.html { redirect_to dish_categories_url, notice: "Dish category was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dish_category
      @dish_category = DishCategory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def dish_category_params
      params.require(:dish_category).permit(:name, :restaurant_id)
    end
end
