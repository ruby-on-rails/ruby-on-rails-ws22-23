class DishesController < ApplicationController
  before_action :set_dish, only: %i[ show edit update destroy ]

  # GET /dishes or /dishes.json
  def index
    @dishes = Dish.all
  end

  # GET /dishes/1 or /dishes/1.json
  def show
  end

  # GET /dishes/new
  def new
    @dish = Dish.new
    @restaurant = Restaurant.find(params[:restaurant_id])
  end

  # GET /dishes/1/edit
  def edit
    @dish = Dish.find(params[:id])
    @restaurant = Restaurant.find(@dish.restaurant.id)
  end

  # POST /dishes or /dishes.json
  def create
    @dish = Dish.new(
      name: dish_params[:name],
      ingredients: dish_params[:ingredients],
      price: dish_params[:price],
      restaurant: Restaurant.find(dish_params[:restaurant_id]),
      dish_category: DishCategory.find(dish_params[:dish_category_id]))

    respond_to do |format|
      if @dish.save
        format.html { redirect_to menu_edit_path(restaurant_id: @dish.restaurant), notice: "Gericht erfolgreich angelegt." }
        format.json { render :show, status: :created, location: @dish }
      end
    end
  end

  # PATCH/PUT /dishes/1 or /dishes/1.json
  def update
    respond_to do |format|
      if @dish.update(dish_params)
        format.html { redirect_to menu_edit_path(restaurant_id: @dish.restaurant), notice: "Gericht erfolgreich bearbeitet." }
        format.json { render :show, status: :ok, location: @dish }
      end
    end
  end

  # DELETE /dishes/1 or /dishes/1.json
  def destroy
    @restaurant = @dish.restaurant
    @notice_text = ""
    unless @dish.line_items.empty?
      @notice_text += "Gericht befindet sich in offenen Bestellungen."
    end
    if @notice_text == ""
      @dish.destroy
      @notice_text = "Gericht erfolgreich gelöscht."
    end

    respond_to do |format|
      format.html { redirect_to menu_edit_path(restaurant_id: @restaurant), notice: @notice_text }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dish
      @dish = Dish.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def dish_params
      params.require(:dish).permit(:name, :ingredients, :price, :restaurant_id, :dish_category_id)
    end
end
