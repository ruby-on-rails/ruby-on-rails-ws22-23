class MenuController < ApplicationController
  include CurrentCart
  def index
    @restaurant = Restaurant.find(params[:restaurant_id])
    @dishes = Dish.where(restaurant: @restaurant)
    all_categories = Dish.where(restaurant: @restaurant).collect do |dish|
      [dish.dish_category.name, dish.dish_category.id]
    end
    @categories = all_categories.uniq
    set_cart(@restaurant.id)
  end

  def edit
    @restaurant = Restaurant.find(params[:restaurant_id])
    @dishes = Dish.where(restaurant: @restaurant)
  end
end
