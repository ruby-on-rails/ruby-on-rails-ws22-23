class OrdersController < ApplicationController
  include CurrentCart
  # before_action :set_cart, only: [:new, :create]
  # before_action :ensure_cart_isnt_empty, only: :new
  before_action :set_order, only: %i[ show edit update destroy cancel accept reject go deliver]

  # GET /orders or /orders.json
  def index
    @orders = Order.where(user: current_user)
  end

  # GET /orders/1 or /orders/1.json
  def show
  end

  # GET /orders/new
  def new
    set_cart(params[:restaurant_id])
    ensure_cart_isnt_empty
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders or /orders.json
  def create
    set_cart(order_params[:restaurant_id])
    @order = Order.new(
      recipient: order_params[:recipient],
      street: order_params[:street],
      house_nr: order_params[:house_nr],
      post_code: order_params[:post_code],
      city: order_params[:city],
      phone: order_params[:phone],
      restaurant: @cart.restaurant,
      user: current_user)
    @order.add_line_items_from_cart(@cart)

    respond_to do |format|
      if @order.save
        # @cart.destroy
        format.html { redirect_to orders_path, notice: "Die Bestellung wurde an das Restaurant weitergeleitet." }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1 or /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to order_url(@order), notice: "Order was successfully updated." }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1 or /orders/1.json
  def destroy
    @restaurant = @order.restaurant
    @order.destroy

    respond_to do |format|
      format.html { redirect_to dashboard_orders_path(restaurant_id: @restaurant), notice: "Bestellung erfolgreich gelöscht." }
      format.json { head :no_content }
    end
  end

  def cancel
    @order.cancel!
    respond_to do |format|
      if @order.save
        format.html { redirect_to orders_path, notice: "Bestellung erfolgreich storniert." }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def accept
    @order.accept!
    respond_to do |format|
      if @order.save
        format.html { redirect_to dashboard_orders_path(restaurant_id: @order.restaurant.id), notice: "Bestellung erfolgreich akzeptiert." }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def reject
    @order.reject!
    respond_to do |format|
      if @order.save
        format.html { redirect_to dashboard_orders_path(restaurant_id: @order.restaurant.id), notice: "Bestellung erfolgreich abgelehnt." }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def go
    @order.go!
    respond_to do |format|
      if @order.save
        format.html { redirect_to dashboard_orders_path(restaurant_id: @order.restaurant.id), notice: "Bestellung erfolgreich losgeschickt." }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  def deliver
    @order.deliver!
    respond_to do |format|
      if @order.save
        format.html { redirect_to dashboard_orders_path(restaurant_id: @order.restaurant.id), notice: "Bestellung erfolgreich ausgeliefert." }
        format.json { head :no_content }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = Order.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def order_params
    params.require(:order).permit(:recipient, :street, :house_nr, :post_code, :city, :phone, :restaurant_id)
  end

  def ensure_cart_isnt_empty
    if @cart.line_items.empty?
      redirect_to restaurants_path, notice: 'Your cart is empty'
    end
  end

end
