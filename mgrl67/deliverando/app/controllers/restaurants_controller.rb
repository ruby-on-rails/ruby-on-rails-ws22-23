class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: %i[ show edit update destroy ]

  # GET /restaurants or /restaurants.json
  def index
    @restaurants_city = ""
    @restaurant_tag = ""
    @restaurants_title = "Alle Restaurants"
    @restaurants = Restaurant.all
    if params[:address] && params[:address] != ""
      @restaurants_city = params[:address]
      @restaurants_title = "Restaurants in #{@restaurants_city}"
      @restaurants = @restaurants.where(address: @restaurants_city)
    end
    if params[:tags] && params[:tags] != ""
      @restaurant_tag = Restaurant.tag_types.key(params[:tags].to_i)
      @restaurants_title = @restaurants_title + ", die #{@restaurant_tag} anbieten"
      @restaurants = @restaurants.where("tags LIKE ?", "%#{@restaurant_tag}%")
    end
  end

  # GET /restaurants/1 or /restaurants/1.json
  def show
    @restaurant = Restaurant.find(params[:restaurant_id])
    @dishes = Dish.where(restaurant: @restaurant)
  end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
  end

  # GET /restaurants/1/edit
  def edit
    @restaurant = Restaurant.find(params[:id])
  end

  # POST /restaurants or /restaurants.json
  def create
    @restaurant = Restaurant.new(restaurant_params)
    @restaurant.user = current_user

    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to dish_new_path(restaurant_id: @restaurant), notice: "Restaurant erfolgreich erstellt. Legen Sie Ihr erstes Gericht an, damit Ihr Restaurant gelistet werden kann." }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1 or /restaurants/1.json
  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to dashboard_path(user_id: current_user), notice: "Restaurant efolgreich bearbeitet." }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1 or /restaurants/1.json
  def destroy
    @notice_text = ""
    unless @restaurant.orders.empty?
      @notice_text += "Das Restaurant hat ausstehende Bestellungen. "
    end
    unless @restaurant.carts.empty?
      @notice_text += "Das Restaurant hat ausstehende Warenkörbe. "
    end
    if @notice_text == ""
      @restaurant.destroy
      @notice_text = "Restaurant erfolgreich gelöscht."
      respond_to do |format|
        format.html { redirect_to dashboard_path(user_id: current_user), notice: @notice_text }
        format.json { head :no_content }
      end
    else
      respond_to do |format|
        format.html { redirect_to edit_restaurant_path(id: @restaurant), notice: @notice_text }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :address, :profile_picture, tags: [])
    end

end
