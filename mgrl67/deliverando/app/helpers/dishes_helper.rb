module DishesHelper
  def categories
    DishCategory.all.collect do |category|
      [category.name, category.id]
    end
  end
end
