class Cart < ApplicationRecord
  has_many :line_items, dependent: :destroy
  belongs_to :restaurant
  belongs_to :user

  def add_dish(dish)
    current_item = line_items.find_by(dish_id: dish.id)
    if current_item
      current_item.quantity += 1
    else
      current_item = line_items.build(dish_id: dish.id)
    end
    current_item
  end
  def total_price
    line_items.to_a.sum(&:total_price)
  end
end