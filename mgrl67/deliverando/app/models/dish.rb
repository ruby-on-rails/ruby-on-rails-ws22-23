class Dish < ApplicationRecord
  belongs_to :restaurant
  has_many :line_items
  belongs_to :dish_category
  before_destroy :ensure_not_referenced_by_any_line_item

  validates :name, presence: true
  validates :ingredients, presence: true
  validates :price, presence: true, numericality: true

  private
  def ensure_not_referenced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, 'Line Items present')
      throw :abort
    end
  end
end
