class LineItem < ApplicationRecord
  belongs_to :order, optional: true
  belongs_to :cart, optional: true
  belongs_to :dish
  def total_price
    dish.price * quantity
  end
end
