class Order < ApplicationRecord
  include WorkflowActiverecord
  belongs_to :user
  belongs_to :restaurant, optional: true
  has_many :line_items, dependent: :destroy

  validates :recipient, presence: true
  validates :street, presence: true
  validates :house_nr, presence: true
  validates :post_code, presence: true, numericality: { only_integer: true }
  validates :city, presence: true
  validates :phone, presence: true

  workflow do
    state :new do
      event :accept, :transitions_to => :accepted
      event :reject, :transitions_to => :rejected
      event :cancel, :transitions_to => :cancelled
    end
    state :accepted do
      event :go, :transitions_to => :on_the_way
      event :cancel, :transitions_to => :cancelled
    end
    state :on_the_way do
      event :deliver, :transitions_to => :final
    end
    state :cancelled
    state :rejected
    state :final
  end

  def status_to_s
    if self.new?
      "Eingegangen"
    else
      if self.accepted?
        "In Zubereitung"
      else
        if self.on_the_way?
          "In Zustellung"
        else
          if self.cancelled?
            "Storniert"
          else
            if self.rejected?
              "Abgelehnt"
            else
              # self.final
              "Zugestellt"
            end
          end
        end
      end
    end
  end

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

  def total_price
    line_items.to_a.sum(&:total_price)
  end
end
