class Restaurant < ApplicationRecord
  has_many :dishes, dependent: :destroy
  has_many :orders
  has_many :carts
  belongs_to :user
  serialize :tags, Array
  has_one_attached :profile_picture, dependent: :destroy

  validates :name, presence: true
  validates :address, presence: true

  enum tag_types: {
    "Pizza" => 0,
    "Burger" => 1,
    "Chinesisch" => 2,
    "Italienisch" => 3,
    "Indisch" => 4,
    "Türkisch" => 5,
    "Deutsch" => 6
  }
end
