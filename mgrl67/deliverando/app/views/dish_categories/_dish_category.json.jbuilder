json.extract! dish_category, :id, :created_at, :updated_at
json.url dish_category_url(dish_category, format: :json)
