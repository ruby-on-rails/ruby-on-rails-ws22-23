json.extract! restaurant, :id, :name, :address, :img_url, :created_at, :updated_at
json.url restaurant_url(restaurant, format: :json)
