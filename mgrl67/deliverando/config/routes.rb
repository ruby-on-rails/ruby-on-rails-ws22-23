Rails.application.routes.draw do
  get 'dashboard/index'
  resources :orders
  resources :line_items
  resources :carts
  resources :dishes
  resources :dish_categories
  resources :restaurants

  get 'menu/index/:restaurant_id', to: 'menu#index', as: 'menu'
  get 'dashboard/index/:user_id', to: 'dashboard#index', as: 'dashboard'
  get 'dashboard/orders/:restaurant_id', to: 'dashboard#orders', as: 'dashboard_orders'
  get 'restaurant/show/:id', to: 'restaurants#show', as: 'show_restaurant'
  get 'menu/edit/:restaurant_id', to: 'menu#edit', as: 'menu_edit'
  get 'dishes/new/:restaurant_id', to: 'dishes#new', as: 'dish_new'
  get 'dish_category/new/:restaurant_id', to: 'dish_categories#new', as: 'dish_category_new'
  get 'orders/new/:restaurant_id', to: 'orders#new', as: 'order_new'
  get 'orders/cancel/:id', to: 'orders#cancel', as: 'order_cancel'
  get 'orders/accept/:id', to: 'orders#accept', as: 'order_accept'
  get 'orders/reject/:id', to: 'orders#reject', as: 'order_reject'
  get 'orders/go/:id', to: 'orders#go', as: 'order_go'
  get 'orders/deliver/:id', to: 'orders#deliver', as: 'order_deliver'
  get 'menu/index'

  devise_for :users
  devise_scope :user do
    authenticated :user do
      root 'restaurants#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
end
