class CreateDishes < ActiveRecord::Migration[6.1]
  def change
    create_table :dishes do |t|
      t.string :name
      t.string :ingredients
      t.decimal :price, precision: 8, scale: 2
      t.references :restaurant, null: false, foreign_key: true

      t.timestamps
    end
  end
end
