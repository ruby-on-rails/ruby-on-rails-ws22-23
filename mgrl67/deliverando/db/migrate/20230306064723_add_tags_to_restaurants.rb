class AddTagsToRestaurants < ActiveRecord::Migration[6.1]
  def change
    add_column :restaurants, :tags, :text, default: "[]"
  end
end
