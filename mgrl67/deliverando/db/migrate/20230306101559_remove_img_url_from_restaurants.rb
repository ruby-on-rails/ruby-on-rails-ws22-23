class RemoveImgUrlFromRestaurants < ActiveRecord::Migration[6.1]
  def change
    remove_column :restaurants, :img_url
  end
end
