class RemoveRoleAndCartFromUser < ActiveRecord::Migration[6.1]
  def change
    remove_column :users, :role
    remove_column :users, :cart_id
  end
end
