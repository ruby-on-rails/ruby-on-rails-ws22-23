class AddAddressToOrder < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :recipient, :string
    add_column :orders, :street, :string
    add_column :orders, :house_nr, :string
    add_column :orders, :post_code, :string
    add_column :orders, :city, :string
    add_column :orders, :phone, :string
  end
end
