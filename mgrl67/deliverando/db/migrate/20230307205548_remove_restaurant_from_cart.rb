class RemoveRestaurantFromCart < ActiveRecord::Migration[6.1]
  def change
    remove_column :carts, :restaurant_id
  end
end
