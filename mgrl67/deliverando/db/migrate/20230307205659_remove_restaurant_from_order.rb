class RemoveRestaurantFromOrder < ActiveRecord::Migration[6.1]
  def change
    remove_column :orders, :restaurant_id
  end
end
