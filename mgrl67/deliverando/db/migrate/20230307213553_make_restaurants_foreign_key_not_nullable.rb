class MakeRestaurantsForeignKeyNotNullable < ActiveRecord::Migration[6.1]
  def change
    change_column :orders, :restaurant_id, :integer,  null: false
  end
end
