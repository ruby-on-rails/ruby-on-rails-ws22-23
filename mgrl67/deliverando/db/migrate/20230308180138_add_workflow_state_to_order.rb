class AddWorkflowStateToOrder < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :workflow_state, :string
  end
end
