class AddDetailsToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :name, :string
    add_column :users, :street, :string
    add_column :users, :house_nr, :string
    add_column :users, :post_code, :string
    add_column :users, :city, :string
    add_column :users, :phone, :string
  end
end
