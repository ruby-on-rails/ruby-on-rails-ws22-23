class AddNameToDishCategories < ActiveRecord::Migration[6.1]
  def change
    add_column :dish_categories, :name, :string
  end
end
