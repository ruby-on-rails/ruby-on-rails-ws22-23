# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

LineItem.delete_all
Order.delete_all
Cart.delete_all
Dish.delete_all
DishCategory.delete_all
Restaurant.delete_all
User.delete_all

rest = User.create! :email => 'rest@mail.com', :password => 'passwort', :password_confirmation => 'passwort'
cust = User.create! :email => 'cust@mail.com', :password => 'passwort', :password_confirmation => 'passwort'
cust.name = 'Max Mustermann'
cust.street = 'Schlossallee'
cust.house_nr = '17A'
cust.post_code = '12345'
cust.city = 'Musterstadt'
cust.phone = '+49 1234 5678901'
cust.save!

restaurant = Restaurant.create(name: "Restaurant", address: "Gießen", tags: %w[Italienisch Pizza], user: rest)
burger_house = Restaurant.create(name: "Burger House", address: "Lich", tags: %w[Burger], user: rest)
indian_restaurant = Restaurant.create(name: "Indian", address: "Gießen", tags: %w[Indisch], user: rest)
restaurant.profile_picture.attach(io: File.open('app/assets/images/restaurant.jpg'), filename: 'restaurant.jpg', content_type: 'image/jpg')
burger_house.profile_picture.attach(io: File.open('app/assets/images/burgerhouse.jpg'), filename: 'burgerhouse.jpg', content_type: 'image/jpg')
indian_restaurant.profile_picture.attach(io: File.open('app/assets/images/indian.jpg'), filename: 'indian.jpg', content_type: 'image/jpg')

starters = DishCategory.create(name: "Vorspeisen")
salads = DishCategory.create(name: "Salate")
mains = DishCategory.create(name: "Hauptspeisen")
sides = DishCategory.create(name: "Beilagen")
desserts = DishCategory.create(name: "Desserts")
drinks = DishCategory.create(name: "Getränke")

Dish.create(name: "Caesar Salat", ingredients: "Salat, Salatsoße", price: 4.49, restaurant: restaurant, dish_category: salads)
Dish.create(name: "Pizza Margherita", ingredients: "Tomatensauce, Mozzarella, Basilikum", price: 6.99, restaurant: restaurant, dish_category: mains)
Dish.create(name: "Pizza Salami", ingredients: "Salami, Tomatensauce, Mozzarella", price: 8.99, restaurant: restaurant, dish_category: mains)
Dish.create(name: "Lasagne", ingredients: "Rinderhackfleisch, Tomatensauce, Bèchamelsauce, Mozzarella", price: 9.99, restaurant: restaurant, dish_category: mains)

cheeseburger = Dish.create(name: "Cheeseburger", ingredients: "Rindfleisch, Salat, Tomaten, Cheddar", price: 6.99, restaurant: burger_house, dish_category: mains)
pommes_frites = Dish.create(name: "Pommes Frites", ingredients: "Kartoffeln", price: 3.99, restaurant: burger_house, dish_category: sides)
coke = Dish.create(name: "Cola", ingredients: "0,75 Liter Coca Cola", price: 3.99, restaurant: burger_house, dish_category: drinks)

Dish.create(name: "Gemüse Pakoras", ingredients: "Gemüse, Dip", price: 5.80, restaurant: indian_restaurant, dish_category: starters)
Dish.create(name: "Chicken Tikka Masala", ingredients: "Hähnchenfleisch, Gemüse, Reis", price: 7.50, restaurant: indian_restaurant, dish_category: mains)
Dish.create(name: "Shrikand", ingredients: "Joghurt, Rosinen, Mandeln", price: 4.00, restaurant: indian_restaurant, dish_category: desserts)

order = Order.create(user: cust, restaurant: burger_house, recipient: cust.name, street: cust.street, house_nr: cust.house_nr, post_code: cust.post_code, city: cust.city, phone: cust.phone)
LineItem.create(dish: cheeseburger, order: order, quantity: 2)
LineItem.create(dish: pommes_frites, order: order)
LineItem.create(dish: coke, order: order)
