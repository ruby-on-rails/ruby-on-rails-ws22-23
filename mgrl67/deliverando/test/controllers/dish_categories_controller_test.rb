require "test_helper"

class DishCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dish_category = dish_categories(:one)
  end

  test "should get index" do
    get dish_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_dish_category_url
    assert_response :success
  end

  test "should create dish_category" do
    assert_difference("DishCategory.count") do
      post dish_categories_url, params: { dish_category: {  } }
    end

    assert_redirected_to dish_category_url(DishCategory.last)
  end

  test "should show dish_category" do
    get dish_category_url(@dish_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_dish_category_url(@dish_category)
    assert_response :success
  end

  test "should update dish_category" do
    patch dish_category_url(@dish_category), params: { dish_category: {  } }
    assert_redirected_to dish_category_url(@dish_category)
  end

  test "should destroy dish_category" do
    assert_difference("DishCategory.count", -1) do
      delete dish_category_url(@dish_category)
    end

    assert_redirected_to dish_categories_url
  end
end
