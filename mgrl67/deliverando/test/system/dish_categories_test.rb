require "application_system_test_case"

class DishCategoriesTest < ApplicationSystemTestCase
  setup do
    @dish_category = dish_categories(:one)
  end

  test "visiting the index" do
    visit dish_categories_url
    assert_selector "h1", text: "Dish categories"
  end

  test "should create dish category" do
    visit dish_categories_url
    click_on "New dish category"

    click_on "Create Dish category"

    assert_text "Dish category was successfully created"
    click_on "Back"
  end

  test "should update Dish category" do
    visit dish_category_url(@dish_category)
    click_on "Edit this dish category", match: :first

    click_on "Update Dish category"

    assert_text "Dish category was successfully updated"
    click_on "Back"
  end

  test "should destroy Dish category" do
    visit dish_category_url(@dish_category)
    click_on "Destroy this dish category", match: :first

    assert_text "Dish category was successfully destroyed"
  end
end
