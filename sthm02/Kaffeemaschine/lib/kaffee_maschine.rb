require_relative 'exceptions'

class KaffeeMaschine

  CupSize = 1

  def initialize
    @wasserfuellstand = 3
    @is_on = false
  end

  def einschalten
    puts("Kaffeemaschine ist eingeschaltet!")
    @is_on = true
  end

  def tassen_fuellen(n, icon)
    iteration = 0
    str = ""

    while iteration < n
      if @wasserfuellstand < 1
        raise NotEnoughWaterLeftError.new 'will not allow to cook coffee if has not enough water in water tank'
      end
      str.concat("#{icon} ")
      iteration += 1
      @wasserfuellstand -= CupSize
    end
    puts(str)
  end

  def koche_kaffee(n, drink:)
    if @is_on == false
      raise NotOnlineError.new 'will not allow to cook coffee if is off'
    end

    dString = drink.to_s

    case dString.chomp
      when "tee"
        tassen_fuellen(n, "🍵")
      when "kaffee"
        tassen_fuellen(n, "☕")
      when "himbeer"
        tassen_fuellen(n, "🍺")
      else
        raise DrinkNotAvailable.new 'drink is not available'
    end
  end
end