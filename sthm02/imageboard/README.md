# ImageBoard:

von Sven Thomas

---

# 1. Projektbeschreibung

Bei dem ImageBoard handelt es sich um eine Prototypen-ähnliche Seite, auf welcher ein Nutzer Bilder-Posts erstellen und 
bearbeiten kann. Andere Nutzer können diese Posts zudem kommentieren, insofern sie über ein registriertes Konto verfügen.

---

# 2. Installation

## 2.1 Anleitung

1. `bundle install`
2. `bin/rails db:migrate`
3. `bin/rails db:seed`
4. `yarn add @rails/webpacker`
5. `bin/rails server`

> **Anmerkung**:
> ggf. muss zusätzlich jQuery manuell mittels `yarn add jquery` installiert werden

Die Seite ist nun auf `localhost:3000/` erreichbar.

## 2.2 Testdaten

Nach der Installation sollte sich bereits ein Testdatensatz auf der Seite befinden.

> Bei den Posts `My First Test Post` sowie `City Tour Pictures #1: Stockholm` sind bereits Nutzerkommentare vorhanden

**Anmeldedaten der Nutzers `mmustermann`:**
* E-Mail-Adresse:
  * max.mustermann@mail.de
* Passwort:
  * test1234

---

# 3. Anmerkungen zur Bedienung
**Suche:**
* Die Suche auf der Homepage sucht ausschließlich nach dem Post-Titel und kann mit den danebenstehenden Filtern kombiniert werden
  * z.B. Ergebnisse für Suchanfrage "test" können nach Erstellungsdatum sortiert werden

**Nutzerprofile:**
* Die Profile der Nutzer sind über das "Mini-Profil" unten links bei einem Post einsehbar
* Das eigene Nutzerprofil kann mit einem Klick auf das oben rechts in der Menüleiste befindliche Mini-Profil des aktuellen Nutzers bearbeitet werden

**Bilddateien:**
* Die Bilddateien werden in `imageboard/public/uploads/` abgelegt
  * der Ordner `post` verweist auf die Post-Thumbnails
  * der Ordner `user` verweist auf die Nutzer-Avatare
* Beim Löschen eines Posts wird auch die verbundene Bilddatei gelöscht, wodurch ein erneutes `bin/rails db:seed` 
scheitern würde. Für diesen Fall gibt es unter `imageboard/app/assets/images` jeweils einen Ordner mit den 
Originaldaten, durch welche die Daten in dem ersten Punkt genannten Ordner ersetzt werden können

**Sichtbarkeit:**
* Jeder Post kann entweder `public` oder `private` sein, im ersten Falle ist er für jeden Nutzer einsehbar und im zweiten nur für den Ersteller auf dessen eigener Home-Seite (="My Posts")
* Die Sichtbarkeit kann in der Post-Bearbeitung jederzeit umgestellt werden

---
# 4. Anforderungen

## Must-Have:
- [X] Registrierung
- [X] Anmeldung
- [X] Bearbeiten von Nutzereinstellungen
- [X] Suchfunktion

### Bildermanagement
- [X] Upload von Bildern mit Titel und Beschreibung
- [X] Editieren von Titel und Beschreibung
- [X] Löschen von Bildern
- [X] Sichtbarkeitseinstellungen

### Kommentarmanagement
- [X] Upload von Kommentaren
- [X] Editieren von Kommentaren + Anmerkung
- [X] Löschen von Kommentaren

## Nice-To-Have:
- [X] Liken von Bildern
- [ ] Favorisieren von Bildern
- [X] Nutzerprofile mit Übersicht
- [ ] "Recommended"-Feed auf Home-Page

---

# 5. Quellen


- https://blog.corsego.com/gem-acts-as-votable-2
  - Verwendet in: Up-/Downvote Button für Posts


- https://thoughtbot.com/blog/how-to-validate-the-presence-of-a-boolean-field-in-a-rails-model
  - Verwendet in: Validierug von boolean-Wert `public`


- https://blog.hubspot.com/website/center-div-css?hubs_content=blog.hubspot.com%2Fwebsite%2Fcenter-div-css&hubs_content-cta=Vertically
  - Verwendet in: CSS-Styling


- https://www.w3schools.com/css/css_grid.asp
  - Verwendet in: CSS-Styling


- https://apidock.com/rails/ActionView/Helpers/UrlHelper/link_to
  - Verwendet in: `link_to`


- https://www.mintbit.com/blog/creating-a-clickable-div-in-rails-views-erb
  - Verwendet in: Klickbare `<div>`


- https://dev.to/eclecticcoding/rails-multiple-seed-files-3422
  - Verwendet in: Anlegen mehrerer Seed-Dateien