class PostsController < ApplicationController
  before_action :set_post, only: %i[ show edit update destroy ]

  # GET /posts or /posts.json
  def index
    @page_title = "ImageBoard: Home"
    @q = Post.ransack(params[:q])
    @posts = @q.result(distinct: true)
    @posts = @posts.select{|post| post.public == true}
  end

  # GET /posts/1 or /posts/1.json
  def show
    @page_title = "ImageBoard: Post-Details"
    @post = Post.find(params[:id])
    @commontable = @post
    commontator_thread_show(@commontable)
    if !@post.public && @post.user != current_user
      redirect_to posts_index_path
    end
  end

  def home
    if current_user
      @page_title = "ImageBoard: My Posts"
      @posts = current_user.posts
    else
      redirect_to new_user_session_path
    end
  end

  def upvote
    @post = Post.find(params[:id])
    if current_user.voted_up_on? @post
      @post.unvote_by current_user
    else
      @post.upvote_by current_user
    end
    render "vote.js.erb"
  end

  def downvote
    @post = Post.find(params[:id])
    if current_user.voted_down_on? @post
      @post.unvote_by current_user
    else
      @post.downvote_by current_user
    end
    render "vote.js.erb"
  end

  # GET /posts/new
  def new
    if current_user
      @post = Post.new
      @page_title = "ImageBoard: New Post"
    else
      redirect_to new_user_session_path
    end
  end

  # GET /posts/1/edit
  def edit
    if @post.user != current_user
      redirect_to posts_index_path
    end
  end

  # POST /posts or /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to post_url(@post), notice: "Post was successfully created." }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to post_url(@post), notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:title)
      params.require(:post).permit(:title, :description, :image, :public)
    end
end
