class UsersController < ApplicationController
  def index
    redirect_to posts_index_path
  end

  def show
    @page_title = "ImageBoard: User-Profile"
    @user = User.find(params[:id])
    @posts = @user.posts
    @posts = @posts.select{|post| post.public == true}
  end
end
