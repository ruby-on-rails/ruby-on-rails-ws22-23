class Post < ApplicationRecord
  mount_uploader :image, ImageUploader

  validates :title, presence: true
  validates :description, presence: true
  validates :public, inclusion: [true, false]

  belongs_to :user
  has_one :commontator_thread, :class_name => 'Commontator::Thread'

  acts_as_commontable dependent: :destroy
  acts_as_votable

  def self.ransackable_attributes(auth_object = nil)
    ["title", "created_at", "weighted_score", "public"]
  end
end
