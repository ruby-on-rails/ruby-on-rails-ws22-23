Rails.application.routes.draw do
  devise_for :users
  get 'users/index'
  match '/users',   to: 'users#index',   via: 'get'
  resources :users, :only =>[:show]
  root 'posts#index', as: 'posts_index'
  resources :posts
  resources :posts do
    member do
      patch "upvote", to: "posts#upvote"
      patch "downvote", to: "posts#downvote"
    end
  end
  match '/home', to: 'posts#home', via: 'get'
  mount Commontator::Engine => '/commontator'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
