class RenamePublicFromPosts < ActiveRecord::Migration[6.1]
  def change
    rename_column :posts, :public?, :public
  end
end
