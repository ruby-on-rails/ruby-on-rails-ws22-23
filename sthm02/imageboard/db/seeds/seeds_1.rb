# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# encoding: utf-8
User.delete_all
mmustermann = User.create!(username: 'mmustermann',
                           email: 'max.mustermann@mail.de',
                           password: 'test1234',
                           password_confirmation: 'test1234',
                           avatar: File.open('public/uploads/user/avatar/thm.png'))

karl123 = User.create!(username: 'karl123',
                       email: 'karl@mail.de',
                       password: 'test1234',
                       password_confirmation: 'test1234',
                       avatar: File.open('public/uploads/user/avatar/karl_avatar.png'))

hugo546 = User.create!(username: 'hugo546',
                       email: 'hugo@mail.de',
                       password: 'test1234',
                       password_confirmation: 'test1234',
                       avatar: File.open('public/uploads/user/avatar/hugo_avatar.png'))

hansi27 = User.create!(username: 'Hansi27',
                       email: 'hansi@mail.de',
                       password: 'test1234',
                       password_confirmation: 'test1234',
                       avatar: File.open('public/uploads/user/avatar/hansi_avatar.jpg'))

test_acc = User.create!(username: 'test_acc',
                        email: 'test@mail.de',
                        password: 'test1234',
                        password_confirmation: 'test1234',
                        avatar: File.open('public/uploads/user/avatar/test_avatar.png'))

peter99 = User.create!(username: 'peter99',
                       email: 'peter@mail.de',
                       password: 'test1234',
                       password_confirmation: 'test1234',
                       avatar: File.open('public/uploads/user/avatar/peter_avatar.png'))

bobo1 = User.create!(username: 'Bobo1',
                     email: 'bobo@mail.de',
                     password: 'test1234',
                     password_confirmation: 'test1234',
                     avatar: File.open('public/uploads/user/avatar/bobo_avatar.png'))

Post.delete_all
post1 = Post.create!(title: 'My First Test Post',
                     description:
                       'This is a test post with a little test description :)',
                     image:
                       File.open('public/uploads/post/image/test.jpg'),
                     user: test_acc,
                     public: true)
# . . .
post2 = Post.create!(title: 'City Tour Pictures #1: Stockholm',
                     description:
                       'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua.',
                     image:
                       File.open('public/uploads/post/image/stockholm.jpg'),
                     user: karl123,
                     public: true)
# . . .
post3 = Post.create!(title: 'A little landscape',
                     description:
                       'this is a test post depicting a nice little landscape.',
                     image:
                       File.open('public/uploads/post/image/landscape.jpeg'),
                     user: mmustermann,
                     public: true)
# . . .
post4 = Post.create!(title: 'Another test post',
                     description:
                       'This post has a particularly long description to test long texts. Lorem ipsum dolor sit amet,
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
ipsum dolor sit amet.',
                     image:
                       File.open('public/uploads/post/image/picture.jpg'),
                     user: hugo546,
                     public: true)
# . . .
post5 = Post.create!(title: 'Holiday Beach Picture',
                     description:
                       'This is a picture of a beach that was totally not just taken from Google :).',
                     image:
                       File.open('public/uploads/post/image/beach.jpg'),
                     user: hansi27,
                     public: true)
# . . .
post6 = Post.create!(title: 'Monkey picture',
                     description:
                       'Monkey picture.',
                     image:
                       File.open('public/uploads/post/image/monkey.jpg'),
                     user: mmustermann,
                     public: true)
# . . .
post7 = Post.create!(title: 'City Tour Pictures #2: London',
                     description:
                       'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua.',
                     image:
                       File.open('public/uploads/post/image/london.jpg'),
                     user: karl123,
                     public: true)
# . . .
post8 = Post.create!(title: 'City Tour Pictures #3: Reykjavík',
                     description:
                       'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua.',
                     image:
                       File.open('public/uploads/post/image/island.png'),
                     user: karl123,
                     public: true)
# . . .
post9 = Post.create!(title: 'Obligatory cat pic',
                     description:
                       'Every Image page has to have at least one cat pic.',
                     image:
                       File.open('public/uploads/post/image/cat.png'),
                     user: bobo1,
                     public: true)
# . . .
post10 = Post.create!(title: 'Cologne Tour 2022 #1',
                     description:
                       'Ac felis donec et odio. Erat pellentesque adipiscing commodo elit.
Id donec ultrices tincidunt arcu non sodales. Quis eleifend quam adipiscing vitae proin sagittis nisl.
Risus in hendrerit gravida rutrum quisque non tellus orci. Donec enim diam vulputate ut pharetra sit amet aliquam.
Aliquam id diam maecenas ultricies. Nulla facilisi nullam vehicula ipsum a. Auctor neque vitae tempus quam pellentesque
nec nam aliquam. Ultrices gravida dictum fusce ut placerat orci. Mi tempus imperdiet nulla malesuada pellentesque elit
eget gravida cum. Duis convallis convallis tellus id interdum.',
                     image:
                       File.open('public/uploads/post/image/cologne1.png'),
                     user: mmustermann,
                      public: true)
# . . .
post11 = Post.create!(title: 'Thunderstorm',
                     description:
                       'Test picture of a thunderstorm.',
                     image:
                       File.open('public/uploads/post/image/thunderstorm.png'),
                     user: peter99,
                      public: true)
# . . .
post12 = Post.create!(title: 'City Tour Pictures #4: Paris',
                     description:
                       'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua.',
                     image:
                       File.open('public/uploads/post/image/paris.png'),
                     user: karl123,
                      public: true)
# . . .
post13 = Post.create!(title: 'Sport picture',
                      description:
                        'Test picture of a soccer game.',
                      image:
                        File.open('public/uploads/post/image/soccer.png'),
                      user: hugo546,
                      public: false)
# . . .
post14 = Post.create!(title: 'Cologne Tour 2022 #2',
                      description:
                        'Ac felis donec et odio. Erat pellentesque adipiscing commodo elit.
Id donec ultrices tincidunt arcu non sodales. Quis eleifend quam adipiscing vitae proin sagittis nisl.
Risus in hendrerit gravida rutrum quisque non tellus orci. Donec enim diam vulputate ut pharetra sit amet aliquam.
Aliquam id diam maecenas ultricies. Nulla facilisi nullam vehicula ipsum a. Auctor neque vitae tempus quam pellentesque
nec nam aliquam. Ultrices gravida dictum fusce ut placerat orci. Mi tempus imperdiet nulla malesuada pellentesque elit
eget gravida cum. Duis convallis convallis tellus id interdum.',
                      image:
                        File.open('public/uploads/post/image/cologne2.png'),
                      user: karl123,
                      public: true)