Commontator::Comment.delete_all
# . . .
Commontator::Comment.create!(thread: Post.find_by_title("My First Test Post").commontator_thread,
                             creator: User.find_by_username("mmustermann"),
                             body: "This is a test comment.")
Commontator::Comment.create!(thread: Post.find_by_title("My First Test Post").commontator_thread,
                             creator: User.find_by_username("hugo546"),
                             body: "This is also a test comment :)")
Commontator::Comment.create!(thread: Post.find_by_title("My First Test Post").commontator_thread,
                             creator: User.find_by_username("test_acc"),
                             body: "This comment is used to show long comments. Ac felis donec et odio. Erat
pellentesque adipiscing commodo elit. Id donec ultrices tincidunt arcu non sodales. Quis eleifend quam adipiscing
vitae proin sagittis nisl. Risus in hendrerit gravida rutrum quisque non tellus orci. Donec enim diam vulputate ut
pharetra sit amet aliquam. Aliquam id diam maecenas ultricies. Nulla facilisi nullam vehicula ipsum a. Auctor neque
vitae tempus quam pellentesque nec nam aliquam. Ultrices gravida dictum fusce ut placerat orci. Mi tempus imperdiet
nulla malesuada pellentesque elit eget gravida cum. Duis convallis convallis tellus id interdum.")
Commontator::Comment.create!(thread: Post.find_by_title("City Tour Pictures #1: Stockholm").commontator_thread,
                             creator: User.find_by_username("Hansi27"),
                             body: "Great pic.")
Commontator::Comment.create!(thread: Post.find_by_title("City Tour Pictures #1: Stockholm").commontator_thread,
                             creator: User.find_by_username("Bobo1"),
                             body: "Dolor sit amet consectetur adipiscing elit ut aliquam purus sit. Urna porttitor
rhoncus dolor purus. Ut aliquam purus sit amet luctus venenatis. Arcu ac tortor dignissim convallis aenean et
tortor at risus..")
Commontator::Comment.create!(thread: Post.find_by_title("City Tour Pictures #1: Stockholm").commontator_thread,
                             creator: User.find_by_username("mmustermann"),
                             body: "Faucibus vitae aliquet nec ullamcorper. Sit amet nisl suscipit adipiscing
bibendum.")
Commontator::Comment.create!(thread: Post.find_by_title("City Tour Pictures #1: Stockholm").commontator_thread,
                             creator: User.find_by_username("Hansi27"),
                             body: "Lorem ipsum dolor sit amet consectetur adipiscing elit duis tristique. Fringilla ut
morbi tincidunt augue interdum velit euismod. Tellus orci ac auctor augue. Feugiat scelerisque varius morbi enim.
Faucibus in ornare quam viverra orci.")
Commontator::Comment.create!(thread: Post.find_by_title("City Tour Pictures #1: Stockholm").commontator_thread,
                             creator: User.find_by_username("karl123"),
                             body: "Ullamcorper sit amet risus nullam eget felis eget nunc.")
Commontator::Comment.create!(thread: Post.find_by_title("Monkey picture").commontator_thread,
                             creator: User.find_by_username("peter99"),
                             body: "Monkey picture.")