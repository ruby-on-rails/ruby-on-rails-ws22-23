class StoreController < ApplicationController
  include CurrentCart
  before_action :set_cart
  skip_before_action :authenticate_user!, only: :index

  def index
    @products = Product.order(:title)
  end
end
