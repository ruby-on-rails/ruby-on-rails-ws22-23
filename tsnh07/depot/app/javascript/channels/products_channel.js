import consumer from "./consumer"

consumer.subscriptions.create("ProductsChannel", {
  connected() {
    console.log("ProductsChannel connected")
    // Called when the subscription is ready for use on the server
  },

  disconnected() {
    console.log("ProductsChannel disconnect")
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    console.log("ProductsChannel received")
    const storeElement = document.querySelector("main.store")
    if (storeElement) {
      storeElement.innerHTML = data.html
    }

    // Called when there's incoming data on the websocket for this channel
  }
});
