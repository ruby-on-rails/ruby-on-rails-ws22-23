class LineItem < ApplicationRecord
  belongs_to :product
  belongs_to :cart, optional: true
  belongs_to :order, optional: true


  def total_price
    product.price * quantity
  end

  # validate :order_or_cart_exclusive
  # def order_or_cart_exclusive
  #   if cart
  #     if order
  #       errors.add(:order, "Line Item can not be in order and cart at the same time.")
  #     end
  #   end
  #
  #   if order
  #     if cart
  #       errors.add(:cart, "Line Item can not be in order and cart at the same time.")
  #     end
  #   end
  # end
end
